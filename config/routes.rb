Rails.application.routes.draw do
  devise_for :users
  root 'listings#index'
  resources :listings, only: :index
  resources :user_listing_likes, only: %i[create destroy]
end
