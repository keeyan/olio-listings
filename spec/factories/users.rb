FactoryBot.define do
  factory :user do
    sequence(:email) { "user#{_1}@example.com" }
    password { 'password' }
  end
end
