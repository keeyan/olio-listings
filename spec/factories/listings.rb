FactoryBot.define do
  factory :listing do
    sequence(:external_id)
  end
end
