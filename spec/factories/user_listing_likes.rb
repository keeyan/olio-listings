FactoryBot.define do
  factory :user_listing_like do
    association(:user)
    association(:listing)
  end
end
