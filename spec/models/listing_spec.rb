require 'rails_helper'

RSpec.describe Listing, type: :model do
  describe '#likes_count' do
    subject { listing.likes_count }

    let!(:listing) { create(:listing) }
    let!(:user_listing_like) { create(:user_listing_like, listing:) }
    let!(:user_listing_like2) { create(:user_listing_like, listing:) }
    let!(:user_listing_like3) { create(:user_listing_like, listing:) }

    it 'returns the number of likes the listing has' do
      expect(subject).to eq(3)
    end
  end
end
