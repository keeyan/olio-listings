require 'rails_helper'

RSpec.describe UserListingLike, type: :model do
  let!(:listing) { create(:listing) }
  let!(:user) { create(:user) }

  it 'does not allow the same user to like a listing twice' do
    user.user_listing_likes.create!(listing:)

    second_like = user.user_listing_likes.create(listing:)
    error = second_like.errors.first
    expect(error.type).to eq(:taken)
    expect(error.attribute).to eq(:listing_id)
  end

  it 'allows another user to like a listing already liked by a user' do
    user.user_listing_likes.create!(listing:)

    user2 = create(:user)
    second_like = user2.user_listing_likes.create(listing:)
    expect(second_like.persisted?).to be(true)
  end
end
