require 'rails_helper'

RSpec.describe FetchListingsFromRemoteService do
  subject { described_class.call(user) }

  let!(:user) { nil }

  context 'when a user is passed' do
    let!(:user) { create(:user) }
    let!(:listing) { create(:listing, external_id: 3_899_631) }
    let!(:user_listing_like) { create(:user_listing_like, user:, listing:) }
    let!(:other_user_listing_like) { create(:user_listing_like, listing:) }

    it 'returns an array of listing hashes' do
      VCR.use_cassette('get_remote_listings') { subject }

      liked_listing = subject.find { _1['id'] == listing.id }
      expect(liked_listing['likes_count']).to eq(2)
      expect(liked_listing['user_listing_like_id']).to eq(user_listing_like.id)
    end
  end

  it 'creates new listings' do
    VCR.use_cassette('get_remote_listings') do
      expect { subject }.to(change { Listing.count })
    end
  end

  context 'when the user is nil' do
    let!(:listing) { create(:listing, external_id: 3_899_631) }
    let!(:user_listing_like) { create(:user_listing_like, listing:) }

    it 'returns an array of listing hashes' do
      VCR.use_cassette('get_remote_listings') { subject }

      liked_listing = subject.find { _1['id'] == listing.id }
      expect(liked_listing['likes_count']).to eq(1)
      expect(liked_listing['user_listing_like_id']).to be_nil
    end
  end
end
