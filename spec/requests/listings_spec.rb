require 'rails_helper'

RSpec.describe 'Listings', type: :request do
  describe 'GET /listings' do
    it 'creates any new listings' do
      VCR.use_cassette('get_remote_listings') do
        expect { get '/listings' }.to(change { Listing.count })
      end

      VCR.use_cassette('get_remote_listings') do
        expect { get '/listings' }.not_to(change { Listing.count })
      end
    end
  end
end
