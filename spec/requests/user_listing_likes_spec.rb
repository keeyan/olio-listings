require 'rails_helper'

RSpec.describe 'UserListingLikes', type: :request do
  describe 'POST /user_listing_likes' do
    subject { post '/user_listing_likes', params: { user_listing_like: { listing_id: listing.id } } }

    before { sign_in(user) }

    let!(:listing) { create(:listing) }
    let!(:user) { create(:user) }

    it 'creates a user_listing_like for the listing' do
      expect { subject }.to change { user.user_listing_likes.count }.by(1)
      user_listing_like = user.user_listing_likes.last
      expect(user_listing_like.listing).to eq(listing)
    end

    context 'when the like already exists' do
      let!(:user_listing_like) { create(:user_listing_like, user:, listing:) }

      it 'returns a bad_request response' do
        expect { subject }.not_to(change { user.user_listing_likes.count })
        expect(response).to have_http_status(:bad_request)
        expect(json_response[:errors][:listing_id]).to include('has already been taken')
      end
    end
  end

  describe 'DELETE /user_listing_likes' do
    before { sign_in(user) }

    let!(:user) { create(:user) }
    let!(:user_listing_like) { create(:user_listing_like, user:) }

    subject { delete "/user_listing_likes/#{user_listing_like.id}", params: { format: :json } }

    it 'deletes the user_listing_like' do
      subject
      expect(UserListingLike.find_by_id(id: user_listing_like)).to be_nil
    end

    context 'when logged in as a different user' do
      let!(:user_listing_like) { create(:user_listing_like) }

      it 'responds with forbidden' do
        expect { subject }.not_to(change { UserListingLike.count })
        expect(response).to have_http_status(:forbidden)
      end
    end
  end
end
