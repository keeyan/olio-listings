class CreateListings < ActiveRecord::Migration[7.0]
  def change
    create_table :listings do |t|
      t.integer :external_id, null: false, index: { unique: true }
      t.timestamps
    end
  end
end
