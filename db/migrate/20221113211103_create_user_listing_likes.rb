class CreateUserListingLikes < ActiveRecord::Migration[7.0]
  def change
    create_table :user_listing_likes do |t|
      t.belongs_to :listing
      t.belongs_to :user
      t.index %w[listing_id user_id], unique: true
      t.timestamps
    end
  end
end
