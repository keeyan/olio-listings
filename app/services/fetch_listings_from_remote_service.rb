class FetchListingsFromRemoteService < ApplicationService
  SOURCE = 'https://s3-eu-west-1.amazonaws.com/olio-staging-images/developer/test-articles-v4.json'.freeze

  def initialize(user)
    @user = user
  end

  def call
    raw_listings = Faraday.get(SOURCE).body
    listings = JSON.parse(raw_listings)
    listings.each { _1['external_id'] = _1.delete('id') }
    create_missing_listings(listings)
    add_real_ids_to_listings(listings)
    add_user_likes_to_listings(listings)
    add_listing_like_counts(listings)

    listings
  end

  private

  attr_reader :user

  def add_user_likes_to_listings(listings)
    return if user.nil?

    likes_by_listing_id =
      user.user_listing_likes.where(listing_id: listings.pluck('id')).to_h { [_1.listing_id, _1.id] }

    listings.each { _1['user_listing_like_id'] = likes_by_listing_id[_1['id']] }
  end

  def create_missing_listings(listings)
    external_ids = listings.pluck('external_id')
    new_listing_external_ids = external_ids - Listing.where(external_id: external_ids).pluck(:external_id)

    new_listings = new_listing_external_ids.map { { external_id: _1 } }

    Listing.upsert_all(new_listings) if new_listings.any?
  end

  def add_real_ids_to_listings(listings_hash)
    external_ids_to_ids =
      Listing.where(external_id: listings_hash.pluck('external_id')).to_h { [_1.external_id, _1.id] }

    listings_hash.each { _1['id'] = external_ids_to_ids[_1['external_id']] }
  end

  def add_listing_like_counts(listings)
    listing_ids_to_likes_count = listings_id_to_likes_count(listings)
    listings.each do |listing|
      listing['likes_count'] = listing_ids_to_likes_count[listing['id']] || 0
    end
  end

  def listings_id_to_likes_count(listings)
    listings_with_counts =
      Listing
      .where(id: listings.pluck('id'))
      .left_joins(:user_listing_likes)
      .group('listings.id')
      .select('COUNT(user_listing_likes.id) AS likes_count, listings.id as listing_id')

    listings_with_counts.to_h { [_1['listing_id'], _1['likes_count']] }
  end
end
