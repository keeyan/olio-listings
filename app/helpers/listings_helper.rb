module ListingsHelper
  def heart_image_tag_for_listing(listing)
    image_tag(
      (listing['user_listing_like_id'] ? 'heart-fill.svg' : 'heart.svg'),
      class: 'heart-icon',
      data: {
        action: 'click->listings#toggle_like', listing_id: listing['id'],
        empty_heart_src: image_url('heart.svg'), filled_heart_src: image_url('heart-fill.svg'),
        user_listing_like_id: listing['user_listing_like_id']
      }
    )
  end
end
