import { Controller } from "@hotwired/stimulus";
import Rails from '@rails/ujs';

export default class extends Controller {
  toggle_like(event) {
    const element = event.target
    if (element.dataset.userListingLikeId == null || element.dataset.userListingLikeId == '') {
      $.post({
        url: "/user_listing_likes",
        dataType: 'json',
        data: {
          user_listing_like: { listing_id: element.dataset.listingId }
        },
        success: (partial) => {
          $(`[data-listing-id="${element.dataset.listingId}"]`).html(partial.data.likes_count);
          element.src = element.dataset.filledHeartSrc;
          element.dataset.userListingLikeId = partial.data.user_listing_like.id;
        }
      });
    } else {
      $.ajax({
        url: "/user_listing_likes/" + element.dataset.userListingLikeId,
        type: 'DELETE',
        dataType: 'json',
        success: (partial) => {
          $(`[data-listing-id="${element.dataset.listingId}"]`).html(partial.data.likes_count);
          element.src = element.dataset.emptyHeartSrc;
          element.dataset.userListingLikeId = '';
        }
      });
    }
  }
}
