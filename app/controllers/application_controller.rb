class ApplicationController < ActionController::Base
  include Pundit::Authorization

  before_action :authenticate_user!

  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  private

  def user_not_authorized
    respond_to do |format|
      format.html do
        flash[:alert] = I18n.t!('authorization.not_authorized')
        redirect_to(request.referrer || root_path)
      end

      format.json do
        render json: { errors: [I18n.t!('authorization.not_authorized')] }, status: :forbidden
      end
    end
  end
end
