class UserListingLikesController < ApplicationController
  before_action :ensure_user_listing_like_exists!, :ensure_user_has_access!, only: :destroy

  def create
    user_listing_like = current_user.user_listing_likes.new(user_listing_like_params)
    if user_listing_like.save
      render json: { data: { user_listing_like:,
                             likes_count: user_listing_like.listing.likes_count } }
    else
      render json: { errors: user_listing_like.errors }, status: :bad_request
    end
  end

  def destroy
    user_listing_like.destroy!

    render json: { data: { likes_count: user_listing_like.listing.likes_count } }
  end

  private

  def user_listing_like
    @user_listing_like ||= UserListingLike.find_by(id: params[:id])
  end

  def ensure_user_listing_like_exists!
    render json: { errors: [I18n.t!('user_listing_likes.not_found')] }, status: :not_found if user_listing_like.nil?
  end

  def ensure_user_has_access!
    authorize user_listing_like, :destroy?
  end

  def user_listing_like_params
    params.require(:user_listing_like).permit(:listing_id)
  end
end
