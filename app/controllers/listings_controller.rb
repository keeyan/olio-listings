class ListingsController < ApplicationController
  skip_before_action :authenticate_user!

  def index
    @listings = FetchListingsFromRemoteService.call(current_user)
  end
end
