class UserListingLikePolicy
  attr_reader :user, :user_listing_like

  def initialize(user, user_listing_like)
    @user = user
    @user_listing_like = user_listing_like
  end

  def destroy?
    user_listing_like.user == user
  end
end
