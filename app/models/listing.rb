class Listing < ApplicationRecord
  has_many :user_listing_likes

  def likes_count
    user_listing_likes.count
  end
end
