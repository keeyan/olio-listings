# OLIO Listings

This is my technical test submission for the OLIO Senior Rails Developer role.

It has a main page with listings received from a remote source each time the page loads.

To begin with you won't be logged in, so will not be able to like any posts.
You can create an account with the buttons at the top.
Once you have, you should be able to click the heart icon to like posts.

## Notes about my approach

I have mostly focussed on the back-end since I have a lot more experience in back-end development.

Unfortunately, for the past couple years I have purely worked as a back-end developer sending JSON data structures to the front-end Angular app.
This means I have fallen behind a little on best practices and latest developments on the Rails front-end ecosystem.

So it took a little more time than expected for me to read up on some changes and I may have not followed best practices in a couple of the front-end related components.

## Setup

To setup the project simply run `bin/setup`, assuming you have Ruby 3.1.2 installed.

You can then visit the homepage at [localhost:3000](http://localhost:3000).

## Developing

Running the specs:

```rb
rspec
```

Running the `rubocop` linter to check the code quality:

```rb
rubocop
```

## How it all works

The project consists of 3 models:

* `User`: Contains information about the user. It uses `Devise` to help with login and sign-up.
* `Listing`: This simply has the `external_id` which comes from the S3 bucket. It does not contain any other information, since that can all change on each page load, meaning storing the info would just be a waste of resources.
* `UserListingLike`: This keeps track of when a user likes a listing. It also prevents duplicate likes from the same user.

The main logic for preparing the listings is in `app/services/fetch_listings_from_remote_service.rb`.
What this will do, is fetch the listings from the remote S3 bucket.
It will then create any missing listings locally so that users can like posts.
After that it will add the total likes for all posts.
Finally it will add the current users likes to all posts so that the heart icon can be displayed correctly.

This service is called from the `ListingsController` which then passes that on to the view which renders all the listings.
The view uses Stimulus JS to listen for users clicking on the heart icon.
Once a user clicks on the icon, if they have already liked it, it will send a `DELETE` request to the `UserListingLikesController` which will delete their like and update the heart icon and like count.
Conversely, if the user has not liked the post yet, it will send a `POST` request to create the like.
